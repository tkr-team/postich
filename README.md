# Postich

1. В файле .env должны содержаться значения переменных POSTGRES_USER=имя_пользователя_postgres, POSTGRES_PASSWORD=пароль_пользователя_postgres, POSTGRES_HOST=адрес_postgres, POSTGRES_PORT=порт_postgres, HTTP_HOST=адрес_хоста, HTTP_PORT=порт_хоста.

2. Для запуска миграций введите следующую команду: `migrate -path ./migrations -database 'postgres://<postgresUser>:<postgresPassword>@<postgresHost>:<postgresPort>/postich?sslmode=<sslStatus>' <up/down>`