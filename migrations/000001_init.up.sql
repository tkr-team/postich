CREATE TABLE "user"
(
    user_id uuid NOT NULL,
    user_firstName text,
    user_lastName text,
    user_username text,
    user_tgId integer NOT NULL,

    PRIMARY KEY (user_id),
    UNIQUE (user_tgId)
);

CREATE TABLE "group"
(
    group_id uuid NOT NULL,
    group_name text NOT NULL,

    PRIMARY KEY (group_id)
);

CREATE TABLE userToGroup
(
    userToGroup_id uuid NOT NULL,
    userToGroup_userId uuid NOT NULL REFERENCES "user",
    userToGroup_groupId uuid NOT NULL REFERENCES "group",

    PRIMARY KEY (userToGroup_id)
);


CREATE TABLE tgInfo
(
    tgInfo_id uuid NOT NULL,
    tgInfo_tgChannelId bigint NOT NULL,
    tgInfo_url text NOT NULL,
    tgInfo_name text NOT NULL,

    PRIMARY KEY (tgInfo_id)
);


CREATE TABLE igInfo
(
    igInfo_id uuid NOT NULL,
    igInfo_token text NOT NULL,
    igInfo_name text NOT NULL,
    igInfo_userName text NOT NULL,

    PRIMARY KEY (igInfo_id)
);


CREATE TABLE vkInfo
(
    vkInfo_id uuid NOT NULL,
    vkInfo_token text NOT NULL,

    PRIMARY KEY (vkInfo_id)
);


CREATE TABLE vkPage
(
    vkPage_id uuid NOT NULL,
    vkPage_vkInfoId uuid NOT NULL REFERENCES vkInfo,
    vkPage_url text NOT NULL,
    vkPage_name text NOT NULL,
    vkPage_pageId integer NOT NULL,

    PRIMARY KEY (vkPage_id)
);


CREATE TABLE socNetwork
(
    socNetwork_id uuid NOT NULL,
    socNetwork_type text NOT NULL,
    socNetwork_serviceVk uuid REFERENCES vkInfo,
    socNetwork_serviceTg uuid REFERENCES tgInfo,
    socNetwork_serviceIg uuid REFERENCES igInfo,
    
    PRIMARY KEY (socNetwork_id)  
);


CREATE TABLE userToSocNetwork
(
    userToSocNetwork_id uuid NOT NULL,
    userToSocNetwork_userId uuid NOT NULL REFERENCES "user",
    userToSocNetwork_socNetworkId uuid NOT NULL REFERENCES socNetwork,

    PRIMARY KEY (userToSocNetwork_id)
);

CREATE TABLE socNetworkToGroup
(
    socNetworkToGroup_id uuid NOT NULL,
    socNetworkToGroup_groupId uuid NOT NULL REFERENCES "group",
    socNetworkToGroup_socNetworkId uuid NOT NULL REFERENCES socNetwork,

    PRIMARY KEY (socNetworkToGroup_id)
);