cd $1

docker login --username $DOCKERHUB_USERNAME --password $DOCKERHUB_PASSWORD
sudo docker-compose pull
sudo docker-compose down
sudo docker-compose up -d
sudo docker cp postich_backend:/app/migrations ~/Postich