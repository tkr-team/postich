package app_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"testing"
	"time"
)

var BASE_URL = "http://167.172.100.209/api/v1/"

type outputFileUpload struct {
	UploadedUrl []string `json:"uploaded_url"`
}

type outputVideoUpload struct {
	Response outputVideoUploadResp `json:"response"`
}

type outputVideoUploadResp struct {
	Count int `json:"count"`
}

func TestApp(t *testing.T) {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	createUserPostBody, _ := json.Marshal(map[string]interface{}{
		"user_firstName": "Azamat",
		"user_lastName":  "",
		"user_tgId":      r1.Int31(),
		"user_username":  "qwwwq",
	})
	createUserResp, err := http.Post(BASE_URL+"users", "application/json", bytes.NewBuffer(createUserPostBody))
	if err != nil || createUserResp.StatusCode != http.StatusCreated {
		log.Fatal("Can't post users" + err.Error())
	}
	var u models.User
	json.NewDecoder(createUserResp.Body).Decode(&u)

	createGroupPostBody, _ := json.Marshal(map[string]string{
		"group_name": fmt.Sprintf("Group %d", r1.Int()),
		"user_id":    u.User_ID.String(),
	})
	createGroupResp, err := http.Post(BASE_URL+"groups", "application/json", bytes.NewBuffer(createGroupPostBody))
	if err != nil || createGroupResp.StatusCode != http.StatusCreated {
		log.Fatal(err)
	}
	var g models.Group
	json.NewDecoder(createGroupResp.Body).Decode(&g)

	createVkSocNetworkPostBody, _ := json.Marshal(map[string]interface{}{
		"group_id":     g.Group_ID,
		"user_id":      u.User_ID,
		"vkInfo_token": "34426c641e8c4ec2aedf07df10d673ceb63b2b573cf3056b981cdc09232a76cc5ec3f067e4f793cb70718",
		"vkPage_url":   "https://vk.com/club202711437",
	})
	createVkSocNetworkResp, err := http.Post(BASE_URL+"socNetworks/VK", "application/json",
		bytes.NewBuffer(createVkSocNetworkPostBody))
	if err != nil || createVkSocNetworkResp.StatusCode != http.StatusCreated {
		log.Fatal(err)
	}

	postVkVideoPostBody, _ := json.Marshal(map[string]interface{}{
		"file_id":  "string",
		"group_id": g.Group_ID,
		"url":      "https://video.twimg.com/ext_tw_video/1390020085612486659/pu/vid/1280x720/e66inRBhSRE01l1D.mp4?tag=12",
	})
	postVkVideoResp, err := http.Post(BASE_URL+"groups/post", "application/json",
		bytes.NewBuffer(postVkVideoPostBody))
	if err != nil || postVkVideoResp.StatusCode != http.StatusOK {
		log.Fatal(err)
	}

	var v outputFileUpload
	json.NewDecoder(createGroupResp.Body).Decode(&v)
	for _, videoUrl := range v.UploadedUrl {
		ur, err := url.Parse(videoUrl)
		if err != nil {
			log.Fatal(err)
		}

		uri := fmt.Sprintf("https://api.vk.com/method/video.get?videos=%s&access_token=%s&v=5.130", ur.Path[6:],
			"34426c641e8c4ec2aedf07df10d673ceb63b2b573cf3056b981cdc09232a76cc5ec3f067e4f793cb70718")

		postVideoResp, err := http.Get(uri)
		if err != nil {
			log.Fatal(err)
		}

		var vu outputVideoUpload
		json.NewDecoder(postVideoResp.Body).Decode(&vu)
		if vu.Response.Count == 0 {
			log.Fatal("No post")
		}
	}

	assert.NoError(t, err)
}
