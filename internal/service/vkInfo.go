package service

import (
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type VkInfoService struct {
	repo repository.VkInfo
}

func NewVkInfoService(repo repository.VkInfo) *VkInfoService {
	return &VkInfoService{repo: repo}
}

func (s *VkInfoService) CreateVkInfo(vkInfo models.VkInfo) error {
	if err := s.repo.Create(vkInfo); err != nil {
		return err
	}
	return nil
}

func (s *VkInfoService) GetVkInfoByServiceId(serviceId uuid.UUID) (models.VkInfo, error) {
	vkInfo, err := s.repo.GetVkInfoByServiceId(serviceId)
	if err != nil {
		return vkInfo, err
	}
	return vkInfo, nil
}
