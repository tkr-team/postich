package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
)

var (
	BASE_VK_API_URL = "https://api.vk.com/method"
	VK_API_VERSION  = "5.130"
)

var MAX_UPLOAD_SIZE int64 = 10 << 20

type VkIdResponse struct {
	Body VkIdResponseBody `json:"response"`
}

type VkIdResponseBody struct {
	PageId int    `json:"object_id"`
	Type   string `json:"type"`
}

type VkNameResponse struct {
	Body []VkNameResponseBody `json:"response"`
}

type VkNameResponseBody struct {
	Name string `json:"name"`
}

type VkVideoResponse struct {
	Body VkVideoResponseBody `json:"response"`
}

type VkVideoResponseBody struct {
	UploadURL string `json:"upload_url"`
}

type VkVideoUploadResponse struct {
	VideoHash string `json:"video_hash"`
	Size      int    `json:"size"`
	OwnerId   int    `json:"owner_id"`
	VideoId   int    `json:"video_id"`
}

type VkPageService struct {
	repo repository.VkPage
}

func NewVkPageService(repo repository.VkPage) *VkPageService {
	return &VkPageService{repo: repo}
}

func (s *VkPageService) CreateVkPage(vkPage models.VkPage) error {
	if err := s.repo.Create(vkPage); err != nil {
		return err
	}
	return nil
}

func (s *VkPageService) GetVkPageIdByScreenName(screenName string, accessToken string) (int, error) {
	var response VkIdResponse

	methodName := "utils.resolveScreenName"

	requestString := fmt.Sprintf("%s/%s?screen_name=%s&access_token=%s&v=%s", BASE_VK_API_URL, methodName,
		screenName, accessToken, VK_API_VERSION)
	r, err := http.Get(requestString)
	if err != nil {
		return 0, err
	}

	defer r.Body.Close()
	err = json.NewDecoder(r.Body).Decode(&response)
	if err != nil {
		return 0, err
	}
	return response.Body.PageId, nil
}

func (s *VkPageService) GetVkPageNameById(pageId int, accessToken string) (pageName string, err error) {
	var response VkNameResponse

	methodName := "groups.getById"

	requestString := fmt.Sprintf("%s/%s?group_id=%d&access_token=%s&v=%s", BASE_VK_API_URL, methodName,
		pageId, accessToken, VK_API_VERSION)
	r, err := http.Get(requestString)
	if err != nil {
		return "", err
	}

	defer r.Body.Close()
	err = json.NewDecoder(r.Body).Decode(&response)
	if err != nil {
		return "", err
	}
	return response.Body[0].Name, nil
}

func (s *VkPageService) GetVkPageScreenName(vkURL string) (screenName string, err error) {
	u, err := url.Parse(vkURL)
	if err != nil {
		return "", err
	}

	return u.Path[1:], nil
}

func (s *VkPageService) GetVkPageByServiceId(serviceId uuid.UUID) (models.VkPage, error) {
	vkPages, err := s.repo.GetVkPageByServiceId(serviceId)
	if err != nil {
		return vkPages, err
	}
	return vkPages, nil
}

func (s *VkPageService) PostVideo(filename string, pageId int, accessToken string) (string, error) {
	uploadURL, err := getVideoPostingUrl(filename, pageId, accessToken)
	if err != nil {
		return "", err
	}

	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	fileWriter, err := bodyWriter.CreateFormFile("video_file", filename)
	if err != nil {
		return "", err
	}

	fh, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer fh.Close()

	fi, err := fh.Stat()
	if err != nil {
		return "", err
	}

	if fi.Size() > MAX_UPLOAD_SIZE {
		if err != nil {
			return "", errors.New("File size is bigger than 10MB.")
		}
	}

	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return "", err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	resp, err := http.Post(uploadURL, contentType, bodyBuf)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	var uploaded VkVideoUploadResponse
	err = json.Unmarshal(body, &uploaded)
	if err != nil {
		return "", err
	}

	u := fmt.Sprintf("https://vk.com/video%d_%d", uploaded.OwnerId, uploaded.VideoId)
	return u, nil
}

func getVideoPostingUrl(fileName string, pageId int, accessToken string) (string, error) {
	var response VkVideoResponse

	methodName := "video.save"

	requestString := fmt.Sprintf("%s/%s?access_token=%s&is_private=0&wallpost=1&group_id=%d&v=%s",
		BASE_VK_API_URL, methodName, accessToken, pageId, VK_API_VERSION)

	r, err := http.Get(requestString)
	if err != nil {
		return "", err
	}

	defer r.Body.Close()
	err = json.NewDecoder(r.Body).Decode(&response)
	if err != nil {
		return "", err
	}

	return response.Body.UploadURL, nil
}
