package service

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
	"net/http"
	"os"
)

var (
	BASE_TG_API_URL = "https://api.telegram.org"
	TG_BOT_TOKEN    = ""
)

type TgInfoService struct {
	repo repository.TgInfo
}

type TgResponse struct {
	OK     bool             `json:"ok"`
	Result TgResponseResult `json:"result"`
}

type TgResponseResult struct {
	Title string `json:"title"`
}

type TgPostResponse struct {
	OK bool `json:"ok"`
	Result TgPostResponseResult `json:"result"`
}

type TgPostResponseResult struct {
	MessageId int `json:"message_id"`
}

func NewTgInfoService(repo repository.TgInfo) *TgInfoService {
	TG_BOT_TOKEN = os.Getenv("TG_BOT_TOKEN")
	return &TgInfoService{repo: repo}
}

func (s *TgInfoService) CreateTgInfo(tgInfo models.TgInfo) error {
	if err := s.repo.Create(tgInfo); err != nil {
		return err
	}
	return nil
}

func (s *TgInfoService) GetTgInfoByServiceId(serviceId uuid.UUID) (models.TgInfo, error) {
	tgInfos, err := s.repo.GetTgInfoByServiceId(serviceId)
	if err != nil {
		return tgInfos, err
	}
	return tgInfos, nil
}

func (s *TgInfoService) GetTgChannelNameById(channelId int64) (string, error) {
	methodName := "getChat"

	requestString := fmt.Sprintf("%s/bot%s/%s", BASE_TG_API_URL, TG_BOT_TOKEN, methodName)

	getChannelNamePostBody, err := json.Marshal(map[string]interface{}{
		"chat_id": channelId,
	})
	if err != nil {
		return "", err
	}

	getChannelNameResponse, err := http.Post(requestString, "application/json",
		bytes.NewBuffer(getChannelNamePostBody))
	if err != nil || getChannelNameResponse.StatusCode != http.StatusOK {
		return "", err
	}

	var tgResponse TgResponse
	err = json.NewDecoder(getChannelNameResponse.Body).Decode(&tgResponse)
	if err != nil {
		return "", err
	}

	return tgResponse.Result.Title, nil
}

func (s *TgInfoService) PostVideo(channelId int64, fileId string) (int, error) {
	methodName := "sendVideo"

	requestString := fmt.Sprintf("%s/bot%s/%s", BASE_TG_API_URL, TG_BOT_TOKEN, methodName)

	postVideoPostBody, err := json.Marshal(map[string]interface{}{
		"chat_id": channelId,
		"video":   fileId,
	})
	if err != nil {
		return 0, err
	}

	postVideoResponse, err := http.Post(requestString, "application/json", bytes.NewBuffer(postVideoPostBody))
	if err != nil || postVideoResponse.StatusCode != http.StatusOK {
		return 0, err
	}

	var tgPostResponse TgPostResponse
	err = json.NewDecoder(postVideoResponse.Body).Decode(&tgPostResponse)
	if err != nil {
		return 0, err
	}
	return tgPostResponse.Result.MessageId, nil
}
