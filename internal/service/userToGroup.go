package service

import (
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type UserToGroupService struct {
	repo repository.UserToGroup
}

func NewUserToGroupService(repo repository.UserToGroup) *UserToGroupService {
	return &UserToGroupService{repo: repo}
}

func (s *UserToGroupService) CreateUserToGroup(userToGroup models.UserToGroup) error {
	if err := s.repo.Create(userToGroup); err != nil {
		return err
	}
	return nil
}
