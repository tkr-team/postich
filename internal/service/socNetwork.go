package service

import (
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type SocNetworkService struct {
	repo repository.SocNetwork
}

func NewSocNetworkService(repo repository.SocNetwork) *SocNetworkService {
	return &SocNetworkService{repo: repo}
}

func (s *SocNetworkService) CreateSocNetwork(socNetwork models.SocNetwork) error {
	if err := s.repo.Create(socNetwork); err != nil {
		return err
	}
	return nil
}

func (s *SocNetworkService) GetSocNetworksByUserId(userId uuid.UUID) ([]models.SocNetwork, error) {
	socNetworks, err := s.repo.GetSocNetworksByUserId(userId)
	if err != nil {
		return nil, err
	}
	return socNetworks, nil
}

func (s *SocNetworkService) GetSocNetworksByGroupId(groupId uuid.UUID) ([]models.SocNetwork, error) {
	socNetworks, err := s.repo.GetSocNetworksByGroupId(groupId)
	if err != nil {
		return nil, err
	}
	return socNetworks, nil
}
