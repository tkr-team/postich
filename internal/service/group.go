package service

import (
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type GroupService struct {
	repo repository.Group
}

func NewGroupService(repo repository.Group) *GroupService {
	return &GroupService{repo: repo}
}

func (s *GroupService) CreateGroup(group models.Group) error {
	if err := s.repo.Create(group); err != nil {
		return err
	}
	return nil
}

func (s *GroupService) GetGroupsByUserId(userId uuid.UUID, pageSize int, pageNum int) ([]models.Group, error) {
	groups, err := s.repo.GetGroupByUserId(userId, pageSize, pageNum)
	if err != nil {
		return nil, err
	}
	return groups, nil
}

func (s *GroupService) CountUserGroups(userId uuid.UUID) (int, error) {
	count, err := s.repo.CountUserGroups(userId)
	if err != nil {
		return count, err
	}
	return count, nil
}
