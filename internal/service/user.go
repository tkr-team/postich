package service

import (
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type UserService struct {
	repo repository.User
}

func NewUserService(repo repository.User) *UserService {
	return &UserService{repo: repo}
}

func (s *UserService) CreateUser(user models.User) error {
	if err := s.repo.Create(user); err != nil {
		return err
	}
	return nil
}

func (s *UserService) GetUserByTgId(tgId int) (models.User, error) {
	user, err := s.repo.GetByTgId(tgId)
	if err != nil {
		return user, err
	}
	return user, nil
}
