package service

import (
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type User interface {
	CreateUser(user models.User) error
	GetUserByTgId(tgId int) (models.User, error)
}

type Group interface {
	CreateGroup(group models.Group) error
	GetGroupsByUserId(userId uuid.UUID, pageSize int, pageNum int) ([]models.Group, error)
	CountUserGroups(userId uuid.UUID) (int, error)
}

type SocNetwork interface {
	CreateSocNetwork(socNetwork models.SocNetwork) error
	GetSocNetworksByUserId(userId uuid.UUID) ([]models.SocNetwork, error)
	GetSocNetworksByGroupId(groupId uuid.UUID) ([]models.SocNetwork, error)
}

type SocNetworkToGroup interface {
	CreateSocNetworkToGroup(socNetworkToGroup models.SocNetworkToGroup) error
}

type IgInfo interface {
	CreateIgInfo(instagramInfo models.IgInfo) error
	GetIgInfoByServiceId(serviceId uuid.UUID) (models.IgInfo, error)
}

type TgInfo interface {
	CreateTgInfo(telegramInfo models.TgInfo) error
	GetTgInfoByServiceId(serviceId uuid.UUID) (models.TgInfo, error)
	GetTgChannelNameById(channelId int64) (string, error)
	PostVideo(channelId int64, fileId string) (int, error)
}

type UserToGroup interface {
	CreateUserToGroup(userToGroup models.UserToGroup) error
}

type UserToSocNetwork interface {
	CreateUserToSocNetwork(userToSocNetwork models.UserToSocNetwork) error
}

type VkInfo interface {
	CreateVkInfo(vkInfo models.VkInfo) error
	GetVkInfoByServiceId(serviceId uuid.UUID) (models.VkInfo, error)
}

type VkPage interface {
	CreateVkPage(vkPage models.VkPage) error
	GetVkPageIdByScreenName(screenName string, accessToken string) (int, error)
	GetVkPageNameById(pageId int, accessToken string) (pageName string, err error)
	GetVkPageScreenName(url string) (screenName string, err error)
	GetVkPageByServiceId(serviceId uuid.UUID) (models.VkPage, error)
	PostVideo(filename string, pageId int, accessToken string) (string, error)
}

type Services struct {
	User              User
	Group             Group
	SocNetwork        SocNetwork
	SocNetworkToGroup SocNetworkToGroup
	TelegramInfo      TgInfo
	InstagramInfo     IgInfo
	UserToGroup       UserToGroup
	UserToSocNetwork  UserToSocNetwork
	VkInfo            VkInfo
	VkPage            VkPage
}

type Dependencies struct {
	Repositories *repository.Repositories
}

func NewServices(deps Dependencies) *Services {
	return &Services{
		User:              NewUserService(deps.Repositories.User),
		Group:             NewGroupService(deps.Repositories.Group),
		SocNetwork:        NewSocNetworkService(deps.Repositories.SocNetwork),
		SocNetworkToGroup: NewSocNetworkToGroupService(deps.Repositories.SocNetworkToGroup),
		TelegramInfo:      NewTgInfoService(deps.Repositories.TelegramInfo),
		InstagramInfo:     NewIgInfoService(deps.Repositories.InstagramInfo),
		UserToGroup:       NewUserToGroupService(deps.Repositories.UserToGroup),
		UserToSocNetwork:  NewUserToSocNetworkService(deps.Repositories.UserToSocNetwork),
		VkInfo:            NewVkInfoService(deps.Repositories.VkInfo),
		VkPage:            NewVkPageService(deps.Repositories.VkPage),
	}
}
