package service

import (
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type SocNetworkToGroupService struct {
	repo repository.SocNetworkToGroup
}

func NewSocNetworkToGroupService(repo repository.SocNetworkToGroup) *SocNetworkToGroupService {
	return &SocNetworkToGroupService{repo: repo}
}

func (s *SocNetworkToGroupService) CreateSocNetworkToGroup(socNetworkToGroup models.SocNetworkToGroup) error {
	if err := s.repo.Create(socNetworkToGroup); err != nil {
		return err
	}
	return nil
}
