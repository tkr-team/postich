package service

import (
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type IgInfoService struct {
	repo repository.IgInfo
}

func NewIgInfoService(repo repository.IgInfo) *IgInfoService {
	return &IgInfoService{repo: repo}
}

func (s *IgInfoService) CreateIgInfo(igInfo models.IgInfo) error {
	if err := s.repo.Create(igInfo); err != nil {
		return err
	}
	return nil
}

func (s *IgInfoService) GetIgInfoByServiceId(serviceId uuid.UUID) (models.IgInfo, error) {
	igInfos, err := s.repo.GetIgInfoByServiceId(serviceId)
	if err != nil {
		return igInfos, err
	}
	return igInfos, nil
}
