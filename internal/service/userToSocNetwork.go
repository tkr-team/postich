package service

import (
	"gitlab.com/tkr-team/postich/internal/models"
	"gitlab.com/tkr-team/postich/internal/repository"
)

type UserToSocNetworkService struct {
	repo repository.UserToSocNetwork
}

func NewUserToSocNetworkService(repo repository.UserToSocNetwork) *UserToSocNetworkService {
	return &UserToSocNetworkService{repo: repo}
}

func (s *UserToSocNetworkService) CreateUserToSocNetwork(userToSocNetwork models.UserToSocNetwork) error {
	if err := s.repo.Create(userToSocNetwork); err != nil {
		return err
	}
	return nil
}
