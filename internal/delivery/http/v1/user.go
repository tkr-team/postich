package v1

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
)

type inputAddUser struct {
	User_firstName string `json:"user_firstName"`
	User_lastName  string `json:"user_lastName"`
	User_username  string `json:"user_username"`
	User_tgId      int    `json:"user_tgId"`
}

func (h *Handler) initUserRoutes(api *gin.RouterGroup) {
	user := api.Group("/users")
	{
		user.POST("", h.addUser)
		user.GET("", h.getUserByTgId)
	}
}

// @Summary Создание записи о пользователе
// @Tags User
// @Description Добавляет информацию о пользователе
// @Accept  json
// @Produce  json
// @Param input body inputAddUser true "user json"
// @Success 201 {object} models.User
// @Failure 400 {object} HTTPResponse
// @Failure 409 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/users [post]
func (h *Handler) addUser(ctx *gin.Context) {
	var inputModel inputAddUser
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addUser: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	_, err := h.services.User.GetUserByTgId(inputModel.User_tgId)
	if err == nil {
		log.Print("User already exists")
		newResponse(ctx, http.StatusConflict, "User already exists")
		return
	}

	model := models.User{
		User_ID:        uuid.New(),
		User_firstName: inputModel.User_firstName,
		User_lastName:  inputModel.User_lastName,
		User_username:  inputModel.User_username,
		User_tgId:      inputModel.User_tgId,
	}

	if err := h.services.User.CreateUser(model); err != nil {
		log.Print("addUser: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create User")
		return
	}

	ctx.JSON(http.StatusCreated, model)
}

// @Summary Получение информации о пользователе по telegram ID
// @Tags User
// @Description Выдаёт информацию о пользователе по telegram ID
// @Accept  json
// @Produce  json
// @Param tgId query int true "Telegram ID"
// @Success 200 {object} models.User
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Failure default {object} HTTPResponse
// @Router /api/v1/users/ [get]
func (h *Handler) getUserByTgId(ctx *gin.Context) {
	tgId := ctx.Query("tgId")
	id, err := strconv.Atoi(tgId)
	if err != nil {
		log.Print("getUserByTgId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: tgId")
		return
	}

	user, err := h.services.User.GetUserByTgId(id)
	if err != nil {
		log.Print("getUserByTgId: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't get user by tgId: "+tgId)
		return
	}

	ctx.JSON(http.StatusOK, user)
}
