package v1

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tkr-team/postich/internal/service"
)

type Handler struct {
	services *service.Services
}

func NewHandler(services *service.Services) *Handler {
	return &Handler{services: services}
}

func (h *Handler) Init(api *gin.RouterGroup) {
	v1 := api.Group("/v1")
	{
		h.initGroupRoutes(v1)
		h.initIgInfoRoutes(v1)
		h.initSocNetworkRoutes(v1)
		h.initTgInfoRoutes(v1)
		h.initUserRoutes(v1)
		h.initVkInfoRoutes(v1)
		h.initVkPageRoutes(v1)
		h.initSocNetworkToGroupRoutes(v1)
	}
}
