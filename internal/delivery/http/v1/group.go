package v1

import (
	"io"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
)

var VIDEO_TYPES = map[string]interface{}{
	"video/mp4":  nil,
	"video/avi":  nil,
	"video/3gp":  nil,
	"video/mpeg": nil,
	"video/mov":  nil,
	"video/mp3":  nil,
	"video/flv":  nil,
	"video/wmv":  nil,
}

type inputAddGroup struct {
	Group_name string    `json:"group_name" binding:"required"`
	User_id    uuid.UUID `json:"user_id" binding:"required"`
}

type outputGetUserGroups struct {
	Groups []models.Group `json:"groups"`
	Count  int            `json:"count"`
}

type outputFileUpload struct {
	UploadedUrl []string `json:"uploaded_url"`
	UploadedMessages []int `json:"uploaded_messages"`
}

type inputPostFileToGroup struct {
	GroupId uuid.UUID `json:"group_id"`
	URL     string    `json:"url"`
	FileId  string    `json:"file_id"`
}

func (h *Handler) initGroupRoutes(api *gin.RouterGroup) {
	group := api.Group("/groups")
	{
		group.POST("", h.addGroups)
		group.GET("", h.getUserGroups)
		group.POST("/post", h.postFileToGroup)
	}
}

// @Summary Создание группы для пользователя
// @Tags Groups
// @Description Создаёт группу по имени и ID пользователя
// @Accept  json
// @Produce  json
// @Param input body inputAddGroup true "group json"
// @Success 201 {object} models.Group
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/groups [post]
func (h *Handler) addGroups(ctx *gin.Context) {
	var inputModel inputAddGroup
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addGroups: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	gModel := models.Group{
		Group_ID:   uuid.New(),
		Group_name: inputModel.Group_name,
	}

	if err := h.services.Group.CreateGroup(gModel); err != nil {
		log.Print("addGroups: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create group")
		return
	}

	ugModel := models.UserToGroup{
		UserToGroup_ID:      uuid.New(),
		UserToGroup_groupId: gModel.Group_ID,
		UserToGroup_userId:  inputModel.User_id,
	}

	if err := h.services.UserToGroup.CreateUserToGroup(ugModel); err != nil {
		log.Print("addGroups: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create usertogroup")
		return
	}

	ctx.JSON(http.StatusCreated, gModel)
}

// @Summary Получение списка групп пользователя с пагинацией
// @Tags Groups
// @Description Получает список групп пользователя
// @Accept  json
// @Produce  json
// @Param userId query string true "User ID"
// @Param pageNum query int true "Page number"
// @Param pageSize query int true "Page size"
// @Success 200 {object} outputGetUserGroups
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/groups [get]
func (h *Handler) getUserGroups(ctx *gin.Context) {
	userIdParam := ctx.Query("userId")
	userId, err := uuid.Parse(userIdParam)
	if err != nil {
		log.Print("getUserGroups: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: userId")
		return
	}

	pageNumParam := ctx.Query("pageNum")
	pageNum, err := strconv.Atoi(pageNumParam)
	if err != nil {
		log.Print("getUserGroups: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: pageNum")
		return
	}

	pageSizeParam := ctx.Query("pageSize")
	pageSize, err := strconv.Atoi(pageSizeParam)
	if err != nil {
		log.Print("getUserGroups: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: pageSize")
		return
	}

	groups, err := h.services.Group.GetGroupsByUserId(userId, pageSize, pageNum-1)
	if err != nil {
		log.Print("getUserGroups: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't get groups by userId: "+userId.String())
		return
	}

	count, err := h.services.Group.CountUserGroups(userId)
	if err != nil {
		log.Print("getUserGroups: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't count groups by userId "+userId.String())
		return
	}

	ctx.JSON(http.StatusOK, outputGetUserGroups{
		Groups: groups,
		Count:  count,
	})
}

// @Summary Публикация медиа в соц.сети в группе
// @Tags Groups
// @Description Постит медиа в соц.сети в группе
// @Accept  json
// @Produce  json
// @Param input body inputPostFileToGroup true "post file json"
// @Success 200 {object} outputFileUpload
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/groups/post [post]
func (h *Handler) postFileToGroup(ctx *gin.Context) {
	var inputModel inputPostFileToGroup
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("postFileToGroup: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	socNetworks, err := h.services.SocNetwork.GetSocNetworksByGroupId(inputModel.GroupId)
	if err != nil {
		log.Print("postFileToGroup: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get users socnetworks by groupId: "+
			inputModel.GroupId.String())
		return
	}

	fileName := uuid.New().String()
	err = downloadFile(inputModel.URL, fileName)
	if err != nil {
		log.Print("postFileToGroup: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't download file: "+
			inputModel.GroupId.String())
		return
	}
	defer os.Remove(fileName)

	chanUploadedVk := make(chan []string)
	chanUploadedTg := make(chan []int)
	var uploaded outputFileUpload
	for _, socNetwork := range socNetworks {
		switch socNetwork.SocNetwork_type {
		case "VK":
			go h.postVideoToVk(ctx, socNetwork, fileName, chanUploadedVk)
			c := <-chanUploadedVk
			if c == nil {
				return
			}
			uploaded.UploadedUrl = append(uploaded.UploadedUrl, c...)
		case "TG":
			go h.postVideoToTg(ctx, socNetwork, inputModel.FileId, chanUploadedTg)
			c := <-chanUploadedTg
			if c == nil {
				return
			}
			uploaded.UploadedMessages = append(uploaded.UploadedMessages, c...)
		case "IG":
			log.Print("Posting is not implemented with socnetwork type " + socNetwork.SocNetwork_type)
			newResponse(ctx, http.StatusBadRequest, "Posting is not implemented with socnetwork type "+socNetwork.SocNetwork_type)
			return
		default:
			log.Print("No service with type " + socNetwork.SocNetwork_type)
			newResponse(ctx, http.StatusBadRequest, "No service with type "+socNetwork.SocNetwork_type)
			return
		}
	}

	ctx.JSON(http.StatusOK, uploaded)
}

func (h *Handler) postVideoToVk(ctx *gin.Context, socNetwork models.SocNetwork, fileName string, ch chan []string) {
	var uploaded []string
	vkPage, err := h.services.VkPage.GetVkPageByServiceId(socNetwork.SocNetwork_serviceVk)
	if err != nil {
		log.Print("postVideoToVK: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get vkPage")
		ch <- nil
	}
	vkInfo, err := h.services.VkInfo.GetVkInfoByServiceId(socNetwork.SocNetwork_serviceVk)
	if err != nil {
		log.Print("postVideoToVK: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get vkInfo")
		ch <- nil
	}

	url, err := h.services.VkPage.PostVideo(fileName, vkPage.VkPage_pageId, vkInfo.VkInfo_token)
	if err != nil {
		log.Print("postVideoToVK: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't post")
		ch <- nil
	}
	uploaded = append(uploaded, url)

	log.Printf("Posted to VK with pageId %d with name %s with url %s",
		vkPage.VkPage_pageId, vkPage.VkPage_name, url)

	ch <- uploaded
}

func (h *Handler) postVideoToTg(ctx *gin.Context, socNetwork models.SocNetwork, fileId string, ch chan []int) {
	var uploaded []int
	tgInfo, err := h.services.TelegramInfo.GetTgInfoByServiceId(socNetwork.SocNetwork_serviceTg)
	if err != nil {
		log.Print("postVideoToTg: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get tgInfo")
		ch <- nil
	}

	msgId, err := h.services.TelegramInfo.PostVideo(tgInfo.TgInfo_tgChannelId, fileId)
	if err != nil {
		log.Print("postVideoToVK: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't post")
		ch <- nil
	}

	uploaded = append(uploaded, msgId)

	log.Printf("Posted to TG to channeld %s file %s", tgInfo.TgInfo_name, fileId)

	ch <- uploaded
}

func downloadFile(URL, fileName string) error {
	//Get the response bytes from the url
	response, err := http.Get(URL)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if response.StatusCode != 200 {
		return err
	}
	//Create a empty file
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	//Write the bytes to the fiel
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}

	return nil
}
