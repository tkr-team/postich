package v1

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
)

type inputAddVkPage struct {
	VkPage_vkInfoId uuid.UUID `json:"vkPage_vkInfoId" binding:"required"`
	VkPage_url      string    `json:"vkPage_url" binding:"required"`
	VkPage_name     string    `json:"vkPage_name" binding:"required"`
	VkPage_pageId   int       `json:"vkPage_pageId" binding:"required"`
}

func (h *Handler) initVkPageRoutes(api *gin.RouterGroup) {
	vkPage := api.Group("/vkPages")
	{
		vkPage.POST("", h.addVkPage)
	}
}

/*
// @Summary Создание записи о странице Vk
// @Tags VkPage
// @Description Добавляет информацию о странице Vk
// @Accept  json
// @Produce  json
// @Param input body inputAddVkPage true "vkPage json"
// @Success 201 {object} models.VkPage
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/vkPages [post]
*/
func (h *Handler) addVkPage(ctx *gin.Context) {
	var inputModel inputAddVkPage
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addVkPage: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	model := models.VkPage{
		VkPage_ID:       uuid.New(),
		VkPage_vkInfoId: inputModel.VkPage_vkInfoId,
		VkPage_url:      inputModel.VkPage_url,
		VkPage_name:     inputModel.VkPage_name,
		VkPage_pageId:   inputModel.VkPage_pageId,
	}

	if err := h.services.VkPage.CreateVkPage(model); err != nil {
		log.Print("addVkPage: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create VkPage")
		return
	}

	ctx.JSON(http.StatusCreated, model)
}
