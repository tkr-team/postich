package v1

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
)

type inputAddTgInfo struct {
	TgInfo_tgChannelId int64    `json:"tgInfo_tgChannelId" binding:"required"`
	TgInfo_url         string `json:"tgInfo_url" binding:"required"`
	TgInfo_name        string `json:"tgInfo_name" binding:"required"`
}

func (h *Handler) initTgInfoRoutes(api *gin.RouterGroup) {
	tgInfo := api.Group("/tgInfos")
	{
		tgInfo.POST("", h.addTgInfo)
	}
}

/*
// @Summary Создание записи о Telegram
// @Tags TgInfo
// @Description Добавляет информацию о Telegram
// @Accept  json
// @Produce  json
// @Param input body inputAddTgInfo true "tgInfo json"
// @Success 201 {object} models.TgInfo
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/tgInfos [post]
*/
func (h *Handler) addTgInfo(ctx *gin.Context) {
	var inputModel inputAddTgInfo
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addTgInfo: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	model := models.TgInfo{
		TgInfo_ID:          uuid.New(),
		TgInfo_tgChannelId: inputModel.TgInfo_tgChannelId,
		TgInfo_url:         inputModel.TgInfo_url,
		TgInfo_name:        inputModel.TgInfo_name,
	}

	if err := h.services.TelegramInfo.CreateTgInfo(model); err != nil {
		log.Print("addTgInfo: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create TelegramInfo")
		return
	}

	ctx.JSON(http.StatusCreated, model)
}
