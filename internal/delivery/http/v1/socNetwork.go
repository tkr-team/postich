package v1

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
)

type inputAddVkSocNetwork struct {
	User_ID      uuid.UUID `json:"user_id" binding:"required"`
	Group_ID     uuid.UUID `json:"group_id" binding:"required"`
	VkInfo_token string    `json:"vkInfo_token" binding:"required"`
	VkPage_url   string    `json:"vkPage_url" binding:"required"`
}

type inputAddTgSocNetwork struct {
	User_ID   uuid.UUID `json:"user_id" binding:"required"`
	Group_ID  uuid.UUID `json:"group_id" binding:"required"`
	URL       string    `json:"url"`
	ChannelId int64     `json:"channel_id"`
}

type resSocNetworksInfo struct {
	SocNetworkId uuid.UUID `json:"soc_network_id"`
	Name         string    `json:"name"`
}

type outputSocNetworksInfo struct {
	SocNetworks []resSocNetworksInfo `json:"soc_networks"`
	Count       int                  `json:"count"`
}

func (h *Handler) initSocNetworkRoutes(api *gin.RouterGroup) {
	socNetwork := api.Group("/socNetworks")
	{
		socNetwork.POST("/VK", h.addVkSocNetwork)
		socNetwork.POST("/TG", h.addTgSocNetwork)
		socNetwork.GET("/byUser", h.getSocNetworksInfoByUserId)
		socNetwork.GET("/byGroup", h.getSocNetworksInfoByGroupId)
	}
}

// @Summary Создание записи о соц. сети ВК
// @Tags SocNetworks
// @Description Добавляет информацию о соц. сети ВК
// @Accept  json
// @Produce  json
// @Param input body inputAddVkSocNetwork true "socnetwork json"
// @Success 201 {object} models.SocNetwork
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/socNetworks/VK [post]
func (h *Handler) addVkSocNetwork(ctx *gin.Context) {
	var inputModel inputAddVkSocNetwork
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	vkInfoModel := models.VkInfo{
		VkInfo_ID:    uuid.New(),
		VkInfo_token: inputModel.VkInfo_token,
	}

	if err := h.services.VkInfo.CreateVkInfo(vkInfoModel); err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+"couldn't create VkInfo")
		return
	}

	screenName, err := h.services.VkPage.GetVkPageScreenName(inputModel.VkPage_url)
	if err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't get screen name by url "+
			inputModel.VkPage_url)
		return
	}

	vkPageId, err := h.services.VkPage.GetVkPageIdByScreenName(screenName, inputModel.VkInfo_token)
	if err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't get pageId")
		return
	}

	vkPageName, err := h.services.VkPage.GetVkPageNameById(vkPageId, inputModel.VkInfo_token)
	if err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't get pageName")
		return
	}

	vkPageModel := models.VkPage{
		VkPage_ID:       uuid.New(),
		VkPage_vkInfoId: vkInfoModel.VkInfo_ID,
		VkPage_url:      inputModel.VkPage_url,
		VkPage_pageId:   vkPageId,
		VkPage_name:     vkPageName,
	}

	if err := h.services.VkPage.CreateVkPage(vkPageModel); err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+"couldn't create VkPage")
		return
	}

	socNetworkModel := models.SocNetwork{
		SocNetwork_ID:        uuid.New(),
		SocNetwork_type:      "VK",
		SocNetwork_serviceVk: vkInfoModel.VkInfo_ID,
	}

	if err := h.services.SocNetwork.CreateSocNetwork(socNetworkModel); err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create SocNetwork")
		return
	}

	socNetworkToGroupModel := models.SocNetworkToGroup{
		SocNetworkToGroup_ID:           uuid.New(),
		SocNetworkToGroup_socNetworkId: socNetworkModel.SocNetwork_ID,
		SocNetworkToGroup_groupId:      inputModel.Group_ID,
	}

	if err := h.services.SocNetworkToGroup.CreateSocNetworkToGroup(socNetworkToGroupModel); err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create SocNetworkToGroup")
		return
	}

	userToSocNetworkModel := models.UserToSocNetwork{
		UserToSocNetwork_ID:           uuid.New(),
		UserToSocNetwork_userId:       inputModel.User_ID,
		UserToSocNetwork_socNetworkId: socNetworkModel.SocNetwork_ID,
	}

	if err := h.services.UserToSocNetwork.CreateUserToSocNetwork(userToSocNetworkModel); err != nil {
		log.Print("addVkSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create UserToSocNetwork")
		return
	}

	ctx.JSON(http.StatusCreated, socNetworkModel)
}

// @Summary Создание записи о соц. сети Телеграм
// @Tags SocNetworks
// @Description Добавляет информацию о соц. сети Телеграм
// @Accept  json
// @Produce  json
// @Param input body inputAddTgSocNetwork true "socnetwork json"
// @Success 201 {object} models.SocNetwork
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/socNetworks/TG [post]
func (h *Handler) addTgSocNetwork(ctx *gin.Context) {
	var inputModel inputAddTgSocNetwork
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addTgSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	channelName, err := h.services.TelegramInfo.GetTgChannelNameById(inputModel.ChannelId)
	if err != nil {
		log.Print("addTgSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get channel name")
		return
	}

	tgInfoModel := models.TgInfo{
		TgInfo_ID:          uuid.New(),
		TgInfo_tgChannelId: inputModel.ChannelId,
		TgInfo_url:         inputModel.URL,
		TgInfo_name:        channelName,
	}

	if err := h.services.TelegramInfo.CreateTgInfo(tgInfoModel); err != nil {
		log.Print("addTgSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+"couldn't create TgInfo")
		return
	}

	socNetworkModel := models.SocNetwork{
		SocNetwork_ID:        uuid.New(),
		SocNetwork_type:      "TG",
		SocNetwork_serviceTg: tgInfoModel.TgInfo_ID,
	}

	if err := h.services.SocNetwork.CreateSocNetwork(socNetworkModel); err != nil {
		log.Print("addTgSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create SocNetwork")
		return
	}

	socNetworkToGroupModel := models.SocNetworkToGroup{
		SocNetworkToGroup_ID:           uuid.New(),
		SocNetworkToGroup_socNetworkId: socNetworkModel.SocNetwork_ID,
		SocNetworkToGroup_groupId:      inputModel.Group_ID,
	}

	if err := h.services.SocNetworkToGroup.CreateSocNetworkToGroup(socNetworkToGroupModel); err != nil {
		log.Print("addTgSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create SocNetworkToGroup")
		return
	}

	userToSocNetworkModel := models.UserToSocNetwork{
		UserToSocNetwork_ID:           uuid.New(),
		UserToSocNetwork_userId:       inputModel.User_ID,
		UserToSocNetwork_socNetworkId: socNetworkModel.SocNetwork_ID,
	}

	if err := h.services.UserToSocNetwork.CreateUserToSocNetwork(userToSocNetworkModel); err != nil {
		log.Print("addTgSocNetwork: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create UserToSocNetwork")
		return
	}

	ctx.JSON(http.StatusCreated, socNetworkModel)
}

// @Summary Получение списка соц.сетей пользователя
// @Tags SocNetworks
// @Description Получает список соц.сетей пользователя по User ID
// @Accept json
// @Produce json
// @Param userId query string true "User ID"
// @Param pageNum query int true "Page number"
// @Param pageSize query int true "Page size"
// @Success 200 {object} outputSocNetworksInfo
// @Failure 400 {object} HTTPResponse
// @Failure 404 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/socNetworks/byUser [get]
func (h *Handler) getSocNetworksInfoByUserId(ctx *gin.Context) {
	userIdParam := ctx.Query("userId")
	userId, err := uuid.Parse(userIdParam)
	if err != nil {
		log.Print("getSocNetworksInfoByUserId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: userId")
		return
	}

	pageNumParam := ctx.Query("pageNum")
	pageNum, err := strconv.Atoi(pageNumParam)
	if err != nil {
		log.Print("getSocNetworksInfoByUserId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: pageNum")
		return
	}

	pageSizeParam := ctx.Query("pageSize")
	pageSize, err := strconv.Atoi(pageSizeParam)
	if err != nil {
		log.Print("getSocNetworksInfoByUserId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: pageSize")
		return
	}

	socNetworks, err := h.services.SocNetwork.GetSocNetworksByUserId(userId)
	if err != nil {
		log.Print("getSocNetworksInfoByUserId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get users socnetworks by userId: "+
			userId.String())
		return
	}

	var res []resSocNetworksInfo

	for _, socNetwork := range socNetworks {
		switch socNetwork.SocNetwork_type {
		case "VK":
			vkPage, err := h.services.VkPage.GetVkPageByServiceId(socNetwork.SocNetwork_serviceVk)
			if err != nil {
				log.Print("getSocNetworksInfoByUserId: " + err.Error())
				newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get vkPage")
				return
			}
			res = append(res, resSocNetworksInfo{
				SocNetworkId: socNetwork.SocNetwork_ID,
				Name:         vkPage.VkPage_name,
			})
		case "TG":
			tgInfo, err := h.services.TelegramInfo.GetTgInfoByServiceId(socNetwork.SocNetwork_serviceTg)
			if err != nil {
				log.Print("getSocNetworksInfoByUserId: " + err.Error())
				newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get tgInfo")
				return
			}
			res = append(res, resSocNetworksInfo{
				SocNetworkId: socNetwork.SocNetwork_ID,
				Name:         tgInfo.TgInfo_name,
			})
		case "IG":
			igInfo, err := h.services.InstagramInfo.GetIgInfoByServiceId(socNetwork.SocNetwork_serviceIg)
			if err != nil {
				log.Print("getSocNetworksInfoByUserId: " + err.Error())
				newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get igInfo")
				return
			}
			res = append(res, resSocNetworksInfo{
				SocNetworkId: socNetwork.SocNetwork_ID,
				Name:         igInfo.IgInfo_name,
			})
		default:
			log.Print("No service with type " + socNetwork.SocNetwork_type)
			newResponse(ctx, http.StatusBadRequest, "No service with type "+socNetwork.SocNetwork_type)
			return
		}
	}

	ctx.JSON(http.StatusOK, outputSocNetworksInfo{
		SocNetworks: paginate(res, (pageNum-1)*pageSize, pageSize),
		Count:       len(res),
	})
}

// @Summary Получение списка соц.сетей в группе
// @Tags SocNetworks
// @Description Получает список соц.сетей в группе по Group ID
// @Accept json
// @Produce json
// @Param groupId query string true "Group ID"
// @Param pageNum query int true "Page number"
// @Param pageSize query int true "Page size"
// @Success 200 {object} outputSocNetworksInfo
// @Failure 400 {object} HTTPResponse
// @Failure 404 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/socNetworks/byGroup [get]
func (h *Handler) getSocNetworksInfoByGroupId(ctx *gin.Context) {
	groupIdParam := ctx.Query("groupId")
	groupId, err := uuid.Parse(groupIdParam)
	if err != nil {
		log.Print("getSocNetworksInfoByGroupId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: groupId")
		return
	}

	pageNumParam := ctx.Query("pageNum")
	pageNum, err := strconv.Atoi(pageNumParam)
	if err != nil {
		log.Print("getSocNetworksInfoByGroupId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: pageNum")
		return
	}

	pageSizeParam := ctx.Query("pageSize")
	pageSize, err := strconv.Atoi(pageSizeParam)
	if err != nil {
		log.Print("getSocNetworksInfoByGroupId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid query param: pageSize")
		return
	}

	socNetworks, err := h.services.SocNetwork.GetSocNetworksByGroupId(groupId)
	if err != nil {
		log.Print("getSocNetworksInfoByGroupId: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get users socnetworks by groupId: "+
			groupId.String())
		return
	}

	var res []resSocNetworksInfo

	for _, socNetwork := range socNetworks {
		switch socNetwork.SocNetwork_type {
		case "VK":
			vkPage, err := h.services.VkPage.GetVkPageByServiceId(socNetwork.SocNetwork_serviceVk)
			if err != nil {
				log.Print("getSocNetworksInfoByGroupId: " + err.Error())
				newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get vkPage")
				return
			}
			res = append(res, resSocNetworksInfo{
				SocNetworkId: socNetwork.SocNetwork_ID,
				Name:         vkPage.VkPage_name,
			})
		case "TG":
			tgInfo, err := h.services.TelegramInfo.GetTgInfoByServiceId(socNetwork.SocNetwork_serviceTg)
			if err != nil {
				log.Print("getSocNetworksInfoByGroupId: " + err.Error())
				newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get tgInfo")
				return
			}
			res = append(res, resSocNetworksInfo{
				SocNetworkId: socNetwork.SocNetwork_ID,
				Name:         tgInfo.TgInfo_name,
			})
		case "IG":
			igInfo, err := h.services.InstagramInfo.GetIgInfoByServiceId(socNetwork.SocNetwork_serviceIg)
			if err != nil {
				log.Print("getSocNetworksInfoByGroupId: " + err.Error())
				newResponse(ctx, http.StatusBadRequest, err.Error()+" couldn't get igInfo")
				return
			}
			res = append(res, resSocNetworksInfo{
				SocNetworkId: socNetwork.SocNetwork_ID,
				Name:         igInfo.IgInfo_name,
			})
		default:
			log.Print("No service with type " + socNetwork.SocNetwork_type)
			newResponse(ctx, http.StatusBadRequest, "No service with type "+socNetwork.SocNetwork_type)
			return
		}
	}

	ctx.JSON(http.StatusOK, outputSocNetworksInfo{
		SocNetworks: paginate(res, (pageNum-1)*pageSize, pageSize),
		Count:       len(res),
	})
}

func paginate(x []resSocNetworksInfo, skip int, size int) []resSocNetworksInfo {
	if skip > len(x) {
		skip = len(x)
	}

	end := skip + size
	if end > len(x) {
		end = len(x)
	}

	return x[skip:end]
}
