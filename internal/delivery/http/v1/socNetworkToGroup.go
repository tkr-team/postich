package v1

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
	"net/http"
)

type inputAddSocNetworkToGroup struct {
	SocNetworkToGroup_socNetworkId uuid.UUID `json:"socNetwork_id" binding:"required"`
	SocNetworkToGroup_groupId      uuid.UUID `json:"group_id" binding:"required"`
}

func (h *Handler) initSocNetworkToGroupRoutes(api *gin.RouterGroup) {
	socNetworkToGroup := api.Group("/socNetworksToGroup")
	{
		socNetworkToGroup.POST("", h.addVkSocNetworkToGroup)
	}
}

// @Summary Привязка соц. сети к группе соц. сетей
// @Tags SocNetworksToGroup
// @Description Добавляет информацию о привязке соц. сети ВК к группе соц. сетей
// @Accept  json
// @Produce  json
// @Param input body inputAddSocNetworkToGroup true "socNetworkToGroup json"
// @Success 201 {object} models.SocNetworkToGroup
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/socNetworksToGroup/ [post]
func (h *Handler) addVkSocNetworkToGroup(ctx *gin.Context) {
	var inputModel inputAddSocNetworkToGroup
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addVkSocNetworkToGroup: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	socNetworkToGroupModel := models.SocNetworkToGroup{
		SocNetworkToGroup_ID:           uuid.New(),
		SocNetworkToGroup_socNetworkId: inputModel.SocNetworkToGroup_socNetworkId,
		SocNetworkToGroup_groupId:      inputModel.SocNetworkToGroup_groupId,
	}

	if err := h.services.SocNetworkToGroup.CreateSocNetworkToGroup(socNetworkToGroupModel); err != nil {
		log.Print("addVkSocNetworkToGroup: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+"couldn't create SocNetworkToGroup")
		return
	}

	ctx.JSON(http.StatusCreated, socNetworkToGroupModel)
}
