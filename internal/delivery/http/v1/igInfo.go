package v1

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
)

type inputAddIgInfo struct {
	IgInfo_token    string `json:"igInfo_token" binding:"required"`
	IgInfo_username string `json:"igInfo_username" binding:"required"`
	IgInfo_name     string `json:"igInfo_name" binding:"required"`
}

func (h *Handler) initIgInfoRoutes(api *gin.RouterGroup) {
	igInfo := api.Group("/igInfos")
	{
		igInfo.POST("", h.addIgInfo)
	}
}

/*
// @Summary Создание записи об Instagram
// @Tags IgInfo
// @Description Добавляет информацию об Instagramm
// @Accept  json
// @Produce  json
// @Param input body inputAddIgInfo true "group json"
// @Success 201 {object} models.IgInfo
// @Failure 400 {object} HTTPResponse
// @Failure 500 {object} HTTPResponse
// @Router /api/v1/igInfos [post]
*/
func (h *Handler) addIgInfo(ctx *gin.Context) {
	var inputModel inputAddIgInfo
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addIgInfo: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	model := models.IgInfo{
		IgInfo_ID:       uuid.New(),
		IgInfo_token:    inputModel.IgInfo_token,
		IgInfo_username: inputModel.IgInfo_username,
		IgInfo_name:     inputModel.IgInfo_name,
	}

	if err := h.services.InstagramInfo.CreateIgInfo(model); err != nil {
		log.Print("addIgInfo: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create IgInfo")
		return
	}

	ctx.JSON(http.StatusCreated, model)
}
