package v1

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/tkr-team/postich/internal/models"
)

type inputAddVkInfo struct {
	VkInfo_token string `json:"vkInfo_token" binding:"required"`
}

func (h *Handler) initVkInfoRoutes(api *gin.RouterGroup) {
	vkInfo := api.Group("/vkInfos")
	{
		vkInfo.POST("", h.addVkInfo)
	}
}

func (h *Handler) addVkInfo(ctx *gin.Context) {
	var inputModel inputAddVkInfo
	if err := ctx.BindJSON(&inputModel); err != nil {
		log.Print("addVkInfo: " + err.Error())
		newResponse(ctx, http.StatusBadRequest, err.Error()+" invalid input body")
		return
	}

	model := models.VkInfo{
		VkInfo_ID:    uuid.New(),
		VkInfo_token: inputModel.VkInfo_token,
	}

	if err := h.services.VkInfo.CreateVkInfo(model); err != nil {
		log.Print("addVkInfo: " + err.Error())
		newResponse(ctx, http.StatusInternalServerError, err.Error()+" couldn't create VkInfo")
		return
	}

	ctx.JSON(http.StatusCreated, model)
}
