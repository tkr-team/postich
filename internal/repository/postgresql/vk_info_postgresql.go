package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type VkInfoRepositoryPostgres struct {
	db *sqlx.DB
}

func NewVkInfoRepositoryPostgres(db *sqlx.DB) *VkInfoRepositoryPostgres {
	return &VkInfoRepositoryPostgres{db}
}

func (r *VkInfoRepositoryPostgres) Create(vkInfo models.VkInfo) error {
	if err := r.db.QueryRow(`INSERT INTO vkInfo (vkInfo_id, vkInfo_token) VALUES ($1, $2) RETURNING vkInfo_id`,
		vkInfo.VkInfo_ID, vkInfo.VkInfo_token).
		Scan(&vkInfo.VkInfo_ID); err != nil {
		return err
	}
	log.Printf("Created vkInfo: %+v", vkInfo)
	return nil
}

func (r *VkInfoRepositoryPostgres) GetVkInfoByServiceId(serviceId uuid.UUID) (models.VkInfo, error) {
	var vkInfo models.VkInfo
	query := `SELECT v.vkinfo_id, v.vkinfo_token FROM vkinfo AS v WHERE v.vkinfo_id = $1`
	if err := r.db.Get(&vkInfo, query, serviceId); err != nil {
		return vkInfo, err
	}

	return vkInfo, nil
}
