package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type GroupRepositoryPostgres struct {
	db *sqlx.DB
}

func NewGroupRepositoryPostgres(db *sqlx.DB) *GroupRepositoryPostgres {
	return &GroupRepositoryPostgres{db}
}

func (r *GroupRepositoryPostgres) Create(group models.Group) error {
	if err := r.db.QueryRow(`INSERT INTO "group" (group_id, group_name) VALUES ($1, $2) RETURNING group_id`,
		group.Group_ID, group.Group_name).
		Scan(&group.Group_ID); err != nil {
		return err
	}
	log.Printf("Created group: %+v", group)
	return nil
}

func (r GroupRepositoryPostgres) GetGroupByUserId(userId uuid.UUID, pageSize int, pageNum int) ([]models.Group, error) {
	var groups []models.Group

	query := `SELECT g.group_id, g.group_name FROM "group" as g INNER JOIN usertogroup as ug ON g.group_id = 
            ug.usertogroup_groupid WHERE ug.usertogroup_userid = $1 LIMIT $2 OFFSET $3`
	if err := r.db.Select(&groups, query, userId, pageSize, pageNum*pageSize); err != nil {
		return nil, err
	}

	return groups, nil
}

func (r GroupRepositoryPostgres) CountUserGroups(userId uuid.UUID) (int, error) {
	var count int
	query := `SELECT COUNT(*) FROM "group" as g INNER JOIN usertogroup as ug ON g.group_id = 
            ug.usertogroup_groupid WHERE ug.usertogroup_userid = $1`
	if err := r.db.Get(&count, query, userId); err != nil {
		return count, err
	}

	return count, nil
}
