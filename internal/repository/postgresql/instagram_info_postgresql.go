package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type IgInfoRepositoryPostgres struct {
	db *sqlx.DB
}

func NewIgInfoRepositoryPostgres(db *sqlx.DB) *IgInfoRepositoryPostgres {
	return &IgInfoRepositoryPostgres{db}
}

func (r *IgInfoRepositoryPostgres) Create(igInfo models.IgInfo) error {
	if err := r.db.QueryRow(`INSERT INTO igInfo (igInfo_id, igInfo_token, igInfo_name, igInfo_userName) 
	VALUES ($1, $2, $3, $4) RETURNING igInfo_id`,
		igInfo.IgInfo_ID, igInfo.IgInfo_token, igInfo.IgInfo_name, igInfo.IgInfo_username).
		Scan(&igInfo.IgInfo_ID); err != nil {
		return err
	}
	log.Printf("Created igInfo: %+v", igInfo)
	return nil
}

func (r *IgInfoRepositoryPostgres) GetIgInfoByServiceId(serviceId uuid.UUID) (models.IgInfo, error) {
	var igInfo models.IgInfo
	query := `SELECT i.iginfo_id, i.iginfo_token. i.iginfo_name, i.iginfo_username FROM iginfo AS i WHERE i.iginfo_id = $1`
	if err := r.db.Get(&igInfo, query, serviceId); err != nil {
		return igInfo, err
	}

	return igInfo, nil
}
