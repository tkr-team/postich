package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type UserToSocNetworkRepositoryPostgres struct {
	db *sqlx.DB
}

func NewUserToSocNetworkRepositoryPostgres(db *sqlx.DB) *UserToSocNetworkRepositoryPostgres {
	return &UserToSocNetworkRepositoryPostgres{db}
}

func (r *UserToSocNetworkRepositoryPostgres) Create(userToSocNetwork models.UserToSocNetwork) error {
	if err := r.db.QueryRow(`INSERT INTO userToSocNetwork (userToSocNetwork_id, userToSocNetwork_userId, userToSocNetwork_socNetworkId)
	VALUES ($1, $2, $3) RETURNING userToSocNetwork_id`,
		userToSocNetwork.UserToSocNetwork_ID, userToSocNetwork.UserToSocNetwork_userId, userToSocNetwork.UserToSocNetwork_socNetworkId).
		Scan(&userToSocNetwork.UserToSocNetwork_ID); err != nil {
		return err
	}
	log.Printf("Created userToSocNetwork: %+v", userToSocNetwork)
	return nil
}
