package repository

import (
	"errors"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type SocNetworkRepositoryPostgres struct {
	db *sqlx.DB
}

func NewSocNetworkRepositoryPostgres(db *sqlx.DB) *SocNetworkRepositoryPostgres {
	return &SocNetworkRepositoryPostgres{db}
}

func (r *SocNetworkRepositoryPostgres) Create(socNetwork models.SocNetwork) error {
	if socNetwork.SocNetwork_serviceVk != uuid.Nil {
		if err := r.db.QueryRow(`INSERT INTO socNetwork (socNetwork_id, socNetwork_type, socNetwork_serviceVk)
		VALUES ($1, $2, $3) RETURNING socNetwork_id`,
			socNetwork.SocNetwork_ID, socNetwork.SocNetwork_type, socNetwork.SocNetwork_serviceVk).
			Scan(&socNetwork.SocNetwork_ID); err != nil {
			return err
		}
		log.Printf("Created SocNetworkVk: %+v", socNetwork)
		return nil
	} else if socNetwork.SocNetwork_serviceTg != uuid.Nil {
		if err := r.db.QueryRow(`INSERT INTO socNetwork (socNetwork_id, socNetwork_type, socNetwork_serviceTg)
		VALUES ($1, $2, $3) RETURNING socNetwork_id`,
			socNetwork.SocNetwork_ID, socNetwork.SocNetwork_type, socNetwork.SocNetwork_serviceTg).
			Scan(&socNetwork.SocNetwork_ID); err != nil {
			return err
		}
		log.Printf("Created SocNetworkTg: %+v", socNetwork)
		return nil
	} else if socNetwork.SocNetwork_serviceIg != uuid.Nil {
		if err := r.db.QueryRow(`INSERT INTO socNetwork (socNetwork_id, socNetwork_type, socNetwork_serviceIg)
		VALUES ($1, $2, $3) RETURNING socNetwork_id`,
			socNetwork.SocNetwork_ID, socNetwork.SocNetwork_type, socNetwork.SocNetwork_serviceIg).
			Scan(&socNetwork.SocNetwork_ID); err != nil {
			return err
		}
		log.Printf("Created SocNetworkIg: %+v", socNetwork)
		return nil
	}
	return errors.New("no reference id")
}

func (r *SocNetworkRepositoryPostgres) GetSocNetworksByUserId(userId uuid.UUID) ([]models.SocNetwork, error) {
	var socNetworks []models.SocNetwork

	query := `SELECT s.socnetwork_id, s.socnetwork_type, s.socnetwork_servicevk, s.socnetwork_servicetg, s.socnetwork_serviceig
	FROM socnetwork as s INNER JOIN usertosocnetwork as us ON s.socnetwork_id = us.usertosocnetwork_socnetworkid 
	WHERE us.usertosocnetwork_userid = $1`

	if err := r.db.Select(&socNetworks, query, userId); err != nil {
		return nil, err
	}

	return socNetworks, nil
}

func (r *SocNetworkRepositoryPostgres) GetSocNetworksByGroupId(groupId uuid.UUID) ([]models.SocNetwork, error) {
	var socNetworks []models.SocNetwork

	query := `SELECT s.socnetwork_id, s.socnetwork_type, s.socnetwork_servicevk, s.socnetwork_servicetg, s.socnetwork_serviceig
	FROM socnetwork as s INNER JOIN socnetworktogroup as sg ON s.socnetwork_id = sg.socnetworktogroup_socnetworkid 
	WHERE sg.socnetworktogroup_groupid = $1`

	if err := r.db.Select(&socNetworks, query, groupId); err != nil {
		return nil, err
	}

	return socNetworks, nil
}
