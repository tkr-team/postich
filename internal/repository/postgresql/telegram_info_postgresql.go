package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type TgInfoRepositoryPostgres struct {
	db *sqlx.DB
}

func NewTgInfoRepositoryPostgres(db *sqlx.DB) *TgInfoRepositoryPostgres {
	return &TgInfoRepositoryPostgres{db}
}

func (r *TgInfoRepositoryPostgres) Create(tgInfo models.TgInfo) error {
	if err := r.db.QueryRow(`INSERT INTO tgInfo (tgInfo_id, tgInfo_tgChannelId, tgInfo_url, tgInfo_name) 
	VALUES ($1, $2, $3, $4) RETURNING tgInfo_id`,
		tgInfo.TgInfo_ID, tgInfo.TgInfo_tgChannelId, tgInfo.TgInfo_url, tgInfo.TgInfo_name).
		Scan(&tgInfo.TgInfo_ID); err != nil {
		return err
	}
	log.Printf("Created tgInfo: %+v", tgInfo)
	return nil
}

func (r *TgInfoRepositoryPostgres) GetTgInfoByServiceId(serviceId uuid.UUID) (models.TgInfo, error) {
	var tgInfo models.TgInfo
	query := `SELECT t.tginfo_id, t.tginfo_tgchannelid, t.tginfo_url, t.tginfo_name FROM tginfo AS t WHERE t.tginfo_id = $1`
	if err := r.db.Get(&tgInfo, query, serviceId); err != nil {
		return tgInfo, err
	}

	return tgInfo, nil
}
