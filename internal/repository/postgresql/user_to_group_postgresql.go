package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type UserToGroupRepositoryPostgres struct {
	db *sqlx.DB
}

func NewUserToGroupRepositoryPostgres(db *sqlx.DB) *UserToGroupRepositoryPostgres {
	return &UserToGroupRepositoryPostgres{db}
}

func (r *UserToGroupRepositoryPostgres) Create(userToGroup models.UserToGroup) error {
	if err := r.db.QueryRow(`INSERT INTO userToGroup (userToGroup_id, userToGroup_userId, userToGroup_groupId)
	VALUES ($1, $2, $3) RETURNING userToGroup_id`,
		userToGroup.UserToGroup_ID, userToGroup.UserToGroup_userId, userToGroup.UserToGroup_groupId).
		Scan(&userToGroup.UserToGroup_ID); err != nil {
		return err
	}
	log.Printf("Created userToGroup: %+v", userToGroup)
	return nil
}
