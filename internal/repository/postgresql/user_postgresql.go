package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type UserRepositoryPostgres struct {
	db *sqlx.DB
}

func NewUserRepositoryPostgres(db *sqlx.DB) *UserRepositoryPostgres {
	return &UserRepositoryPostgres{db}
}

func (r *UserRepositoryPostgres) Create(user models.User) error {
	if err := r.db.QueryRow(`INSERT INTO "user" (user_id, user_firstName, user_lastName, user_username, user_tgId)
	VALUES ($1, $2, $3, $4, $5) RETURNING user_id`,
		user.User_ID, user.User_firstName, user.User_lastName, user.User_username, user.User_tgId).
		Scan(&user.User_ID); err != nil {
		return err
	}
	log.Printf("Created user: %+v", user)
	return nil
}

func (r *UserRepositoryPostgres) GetByTgId(tgId int) (models.User, error) {
	var user models.User
	query := `SELECT u.user_id, u.user_firstName, u.user_lastName, u.user_username, u.user_tgId FROM "user" AS 
    u WHERE u.user_tgId = $1`

	if err := r.db.Get(&user, query, tgId); err != nil {
		return user, err
	}

	return user, nil
}
