package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type SocNetworkToGroupRepositoryPostgres struct {
	db *sqlx.DB
}

func NewSocNetworkToGroupRepositoryPostgres(db *sqlx.DB) *SocNetworkToGroupRepositoryPostgres {
	return &SocNetworkToGroupRepositoryPostgres{db}
}

func (r *SocNetworkToGroupRepositoryPostgres) Create(socNetworkToGroup models.SocNetworkToGroup) error {
	if err := r.db.QueryRow(`INSERT INTO socnetworktogroup (socnetworktogroup_id, socnetworktogroup_groupid, socnetworktogroup_socnetworkid)
	VALUES ($1, $2, $3) RETURNING socnetworktogroup_id`,
		socNetworkToGroup.SocNetworkToGroup_ID, socNetworkToGroup.SocNetworkToGroup_groupId, socNetworkToGroup.SocNetworkToGroup_socNetworkId).
		Scan(&socNetworkToGroup.SocNetworkToGroup_ID); err != nil {
		return err
	}
	log.Printf("Created SocNetworkToGroup: %+v", socNetworkToGroup)
	return nil
}
