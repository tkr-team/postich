package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	"log"
)

type VkPageRepositoryPostgres struct {
	db *sqlx.DB
}

func NewVkPageRepositoryPostgres(db *sqlx.DB) *VkPageRepositoryPostgres {
	return &VkPageRepositoryPostgres{db}
}

func (r *VkPageRepositoryPostgres) Create(vkPage models.VkPage) error {
	if err := r.db.QueryRow(`INSERT INTO vkPage (vkPage_id, vkPage_vkInfoId, vkPage_url, vkPage_name, vkPage_pageId)
	VALUES ($1, $2, $3, $4, $5) RETURNING vkPage_id`,
		vkPage.VkPage_ID, vkPage.VkPage_vkInfoId, vkPage.VkPage_url, vkPage.VkPage_name, vkPage.VkPage_pageId).
		Scan(&vkPage.VkPage_ID); err != nil {
		return err
	}
	log.Printf("Created vkPage: %+v", vkPage)
	return nil
}

func (r *VkPageRepositoryPostgres) GetVkPageByServiceId(serviceId uuid.UUID) (models.VkPage, error) {
	var vkPage models.VkPage
	query := `SELECT vp.vkpage_id, vp.vkpage_vkinfoid, vp.vkpage_url, vp.vkpage_name, vp.vkpage_pageid FROM vkpage as 
		vp INNER JOIN vkinfo as vi ON vp.vkpage_vkinfoid = vi.vkinfo_id WHERE vi.vkinfo_id = $1`
	if err := r.db.Get(&vkPage, query, serviceId); err != nil {
		return vkPage, err
	}

	return vkPage, nil
}
