package repository

import (
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"gitlab.com/tkr-team/postich/internal/models"
	repository "gitlab.com/tkr-team/postich/internal/repository/postgresql"
)

type User interface {
	Create(user models.User) error
	GetByTgId(tgId int) (models.User, error)
}

type Group interface {
	Create(group models.Group) error
	GetGroupByUserId(userId uuid.UUID, pageSize int, pageNum int) ([]models.Group, error)
	CountUserGroups(userId uuid.UUID) (int, error)
}

type SocNetwork interface {
	Create(socNetwork models.SocNetwork) error
	GetSocNetworksByUserId(userId uuid.UUID) ([]models.SocNetwork, error)
	GetSocNetworksByGroupId(groupId uuid.UUID) ([]models.SocNetwork, error)
}

type SocNetworkToGroup interface {
	Create(socNetworkToGroup models.SocNetworkToGroup) error
}

type IgInfo interface {
	Create(instagramInfo models.IgInfo) error
	GetIgInfoByServiceId(serviceId uuid.UUID) (models.IgInfo, error)
}

type TgInfo interface {
	Create(telegramInfo models.TgInfo) error
	GetTgInfoByServiceId(serviceId uuid.UUID) (models.TgInfo, error)
}

type UserToGroup interface {
	Create(userToGroup models.UserToGroup) error
}

type UserToSocNetwork interface {
	Create(userToSocNetwork models.UserToSocNetwork) error
}

type VkInfo interface {
	Create(vkInfo models.VkInfo) error
	GetVkInfoByServiceId(serviceId uuid.UUID) (models.VkInfo, error)
}

type VkPage interface {
	Create(vkPage models.VkPage) error
	GetVkPageByServiceId(serviceId uuid.UUID) (models.VkPage, error)
}

type Repositories struct {
	User              User
	Group             Group
	SocNetwork        SocNetwork
	SocNetworkToGroup SocNetworkToGroup
	TelegramInfo      TgInfo
	InstagramInfo     IgInfo
	UserToGroup       UserToGroup
	UserToSocNetwork  UserToSocNetwork
	VkInfo            VkInfo
	VkPage            VkPage
}

func NewRepositories(db *sqlx.DB) *Repositories {
	return &Repositories{
		User:              repository.NewUserRepositoryPostgres(db),
		Group:             repository.NewGroupRepositoryPostgres(db),
		SocNetwork:        repository.NewSocNetworkRepositoryPostgres(db),
		SocNetworkToGroup: repository.NewSocNetworkToGroupRepositoryPostgres(db),
		TelegramInfo:      repository.NewTgInfoRepositoryPostgres(db),
		InstagramInfo:     repository.NewIgInfoRepositoryPostgres(db),
		UserToGroup:       repository.NewUserToGroupRepositoryPostgres(db),
		UserToSocNetwork:  repository.NewUserToSocNetworkRepositoryPostgres(db),
		VkInfo:            repository.NewVkInfoRepositoryPostgres(db),
		VkPage:            repository.NewVkPageRepositoryPostgres(db),
	}
}
