package models

import "github.com/google/uuid"

type UserToSocNetwork struct {
	UserToSocNetwork_ID           uuid.UUID `json:"userToSocNetwork_id" binding:"required"`
	UserToSocNetwork_userId       uuid.UUID `json:"userToSocNetwork_userId" binding:"required"`
	UserToSocNetwork_socNetworkId uuid.UUID `json:"userToSocNetwork_socNetworkId" binding:"required"`
}
