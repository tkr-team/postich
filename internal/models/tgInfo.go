package models

import "github.com/google/uuid"

type TgInfo struct {
	TgInfo_ID          uuid.UUID `json:"tgInfo_id" binding:"required"`
	TgInfo_tgChannelId int64     `json:"tgInfo_tgChannelId" binding:"required"`
	TgInfo_url         string    `json:"tgInfo_url" binding:"required"`
	TgInfo_name        string    `json:"tgInfo_name" binding:"required"`
}
