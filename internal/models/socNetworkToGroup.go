package models

import "github.com/google/uuid"

type SocNetworkToGroup struct {
	SocNetworkToGroup_ID           uuid.UUID `json:"socNetworkToGroup_id" binding:"required"`
	SocNetworkToGroup_socNetworkId uuid.UUID `json:"socNetworkToGroup_socNetworkId" binding:"required"`
	SocNetworkToGroup_groupId      uuid.UUID `json:"socNetworkToGroup_groupId" binding:"required"`
}
