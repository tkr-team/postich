package models

import "github.com/google/uuid"

type Group struct {
	Group_ID   uuid.UUID `json:"group_id" binding:"required"`
	Group_name string    `json:"group_name" binding:"required"`
}
