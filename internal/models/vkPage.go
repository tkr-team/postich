package models

import "github.com/google/uuid"

type VkPage struct {
	VkPage_ID       uuid.UUID `json:"vkPage_id" binding:"required"`
	VkPage_vkInfoId uuid.UUID `json:"vkPage_vkInfoId" binding:"required"`
	VkPage_url      string    `json:"vkPage_url" binding:"required"`
	VkPage_name     string    `json:"vkPage_name" binding:"required"`
	VkPage_pageId   int       `json:"vkPage_pageId" binding:"required"`
}
