package models

import "github.com/google/uuid"

type IgInfo struct {
	IgInfo_ID       uuid.UUID `json:"igInfo_id" binding:"required"`
	IgInfo_token    string    `json:"igInfo_token" binding:"required"`
	IgInfo_username string    `json:"igInfo_username" binding:"required"`
	IgInfo_name     string    `json:"igInfo_name" binding:"required"`
}
