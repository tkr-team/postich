package models

import "github.com/google/uuid"

type UserToGroup struct {
	UserToGroup_ID      uuid.UUID `json:"userToGroup_id" binding:"required"`
	UserToGroup_userId  uuid.UUID `json:"userToGroup_userId" binding:"required"`
	UserToGroup_groupId uuid.UUID `json:"userToGroup_groupId" binding:"required"`
}
