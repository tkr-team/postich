package models

import "github.com/google/uuid"

type User struct {
	User_ID        uuid.UUID `json:"user_id" binding:"required"`
	User_firstName string    `json:"user_firstName"`
	User_lastName  string    `json:"user_lastName"`
	User_username  string    `json:"user_username"`
	User_tgId      int       `json:"user_tgId" binding:"required"`
}
