package models

import "github.com/google/uuid"

type VkInfo struct {
	VkInfo_ID    uuid.UUID `json:"vkInfo_id" binding:"required"`
	VkInfo_token string    `json:"vkInfo_token" binding:"required"`
}
