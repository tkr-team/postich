package models

import "github.com/google/uuid"

type SocNetwork struct {
	SocNetwork_ID        uuid.UUID `json:"socNetwork_id" binding:"required"`
	SocNetwork_type      string    `json:"socNetwork_type"`
	SocNetwork_serviceVk uuid.UUID `json:"socNetwork_serviceVkId"`
	SocNetwork_serviceTg uuid.UUID `json:"socNetwork_serviceTgId"`
	SocNetwork_serviceIg uuid.UUID `json:"socNetwork_serviceIgId"`
}
