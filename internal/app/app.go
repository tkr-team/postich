package app

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/tkr-team/postich/internal/delivery/http"
	"gitlab.com/tkr-team/postich/internal/repository"
	"gitlab.com/tkr-team/postich/internal/server"
	"gitlab.com/tkr-team/postich/internal/service"

	"gitlab.com/tkr-team/postich/internal/config"
	"gitlab.com/tkr-team/postich/internal/database/postgresql"
)

func Run(configPath string) {
	cfg, err := config.Init(configPath)
	if err != nil {
		log.Fatal(err)
		return
	}

	db, err := postgresql.NewPostgresDB(cfg.Postgresql)
	if err != nil {
		log.Fatal(err)
		return
	}

	repositories := repository.NewRepositories(db)

	services := service.NewServices(service.Dependencies{Repositories: repositories})
	handlers := http.NewHandler(services)

	server := server.NewServer(cfg, handlers.Init(cfg.HTTP.Host, cfg.HTTP.Port))
	go func() {
		if err := server.Run(); err != nil {
			log.Printf("error occurred while running http server: %s\n", err.Error())
		}
	}()

	log.Print("Server started")

	// Graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)

	// Block until a signal is received.
	sig := <-c
	log.Println("Got signal:", sig)
	log.Print("Shutting down server")

	ctx, shutdown := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdown()

	server.Stop(ctx)
}
