package main

import "gitlab.com/tkr-team/postich/internal/app"

const configPath = "configs/config"

// @title Postich
// @version 0.1
// @description Postich Backend.
func main() {
	app.Run(configPath)
}
